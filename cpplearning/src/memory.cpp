#include <iostream>
#include <string>

using namespace std;

void
l(const string &str){  cout << "Calling " << str << endl; }

struct Account {
  int balance{};
  void d() const { cout << "Account balance is " << balance << endl; }
  void setBalance(const int& b) {
    balance = b;
  }

  const string& getHolder() const{ return holder; }
  
  
  Account(){ l("default ctor"); }
  Account(const Account&){l("copy ctor");}
  Account(Account&&){l("clone ctor");}
  Account& operator=(const Account&){l("assignement operator"); return *this; }
  Account& operator=(Account&&){l("clone ass."); return *this; }
  //  Account& operator+(Account& other){l("addition"); this->balance+=other.balance; }
  
private:
  string holder{"aze"};
};


struct Bank{
  const Account& getAccount()const  { return acc; }
  
private:
  Account acc{};
};

void
do_something(int i)
{
  cout << i << endl;
}

void
do_something(Account *a)
{
  a->balance++;
  a->d();
}

void
do_something_on_3rd(Account *a)
{
  auto b = a+2;
  b->balance++;
}

void
print_account_array(const Account *a, int len)
{
  for (int i=0; i<len; ++i)
    {
      
      cout << "Account "<< i << " ";
      a->d();
      a++;
    }
}

int
main()
{
  auto b = Bank();
  auto& a = b.getAccount();
  a.balance = 50;
  
  //  auto* i = new int();
  //  auto aa = b.getAccount();
}
