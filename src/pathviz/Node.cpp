#include "Node.hpp"

// Based on a 1200x800 sreen

#include <stdlib.h>     /* srand, rand */


constexpr int right_panel_width = 250; //!< Removed from randomized position

/** Create a random node
  *
  * x and y are randomly choosen
  *
  */
Node::Node()
{
  x = rand() % (1100 + 50 - right_panel_width); // 50 -> 1150 - right panel
  y = rand() % 700 + 50;  // 50 ->  750
}
