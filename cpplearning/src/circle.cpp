#include <vector>
#include <iostream>
#include <memory>    // USES make_shared<>

using namespace std;

struct Shape
{
  Shape(int r):val(r) {}

  friend std::ostream& operator<< (std::ostream& stream, const Shape& s)
  {
    stream << "Shape<" << s.val << ">";
    return stream;
  }
  
  virtual bool operator==(const Shape& other) const
  {
    return val == other.val;
  }

  int val;

};

struct Circle : public Shape
{
  Circle(int r):Shape(r) {

  }
  

};

vector<Shape*>
load_shapes()
{
  vector<Shape*> v;
  return v;
};


int
main()
{

  
  // C++98
  /*
  Circle *p = new Circle(42);
  vector<Shape*> v = load_shapes();
  v.push_back(new Circle(42));
  v.push_back(p);
  
  for (vector<Shape*>::iterator i = v.begin(); i!= v.end(); ++i)
    if (*i && **i == *p)
      cout << **i << " is a match\n";

  // Later on and possibly elsewhere
  for (vector<Shape*>::iterator i = v.begin(); i!= v.end(); ++i)
    delete *i;

  delete p;
  */


  
  // Modern C++
  auto p = make_shared<Circle>( 42 );
  auto v = load_shapes();  // shared_ptr<Shape*>
  v.push_back(p.get());    // Shape*
  
  for (auto& s: v)
    if (s&& *s==*p)
      cout << *s << " is a match\n";


  vector<shared_ptr<Shape>> vc;
  
}
