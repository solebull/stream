#include "MainWindow.hpp"

#include <iostream>  // USES std::cin
#include <climits>   // USES INT_MAX
#include <math.h>    // USES ceil()
#include <sstream>
#include <string>

#include "Text.hpp"
#include "Buffer.hpp"
#include "fdMandelbrot.hpp"

#define BG 0x20, 0x20, 0x20

constexpr int window_width = 800;
constexpr int window_height = 600;

/** Construct the SDL main window
  *
  * May thow an exception in case of SDL initialization eror. In this case,
  * the status code can be set to 134.
  *
  * \param argc Number of command line arguments from main.
  * \param argc Number of command line arguments from main.
  *
  */
MainWindow::MainWindow(int argc, char** argv):
  window(nullptr),
  font(nullptr),
  windowRenderer(nullptr),
  fps_update_ms(400),
  fps_update_delta(INT_MAX),
  fpsText(nullptr),
  buffer(nullptr),
  actuelDrawerIndex(0),
  dirty(true)
{
  SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

  // Create an application window with the following settings:
  window = SDL_CreateWindow("An SDL2 window",                  // window title
		    SDL_WINDOWPOS_UNDEFINED,           // initial x position
		    SDL_WINDOWPOS_UNDEFINED,           // initial y position
		    window_width,                      // width, in pixels
		    window_height,                     // height, in pixels
		    SDL_WINDOW_OPENGL                  // flags - see below
			    );

  // Check that the window was successfully created
  if (window == nullptr)
    {
      std::ostringstream oss;
      oss << "Could not create window: " << SDL_GetError();

      // In the case that the window could not be made...
      std::cerr << "Could not create window: " << SDL_GetError() << std::endl;
      throw std::runtime_error(oss.str());
    }


  // TTF  
  if (TTF_Init() == 0)
    {
      font = TTF_OpenFont("../../media/lazy.ttf", 22);
      if( !font )
	throw std::runtime_error("Can't open font file");
    }
  else
    {
      std::ostringstream oss;
      oss << "Could not initialize TTF: " << SDL_GetError();
      
      // In the case that the window could not be made...
      std::cerr << "Could not create window: " << TTF_GetError() << std::endl;
      throw std::runtime_error(oss.str());
    }

  windowRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  buffer = new Buffer(argc, argv, windowRenderer, window_width, window_height);

  drawers.push_back(new fdMandelbrot());
}

/** Destroy the SDL window and exit SDL
  *
  */
MainWindow::~MainWindow()
{
  delete buffer;
  
  TTF_Quit();

  // Close and destroy the window
  SDL_DestroyWindow(window);

  // Clean up
  SDL_Quit();
}

/** Enter the game loop
  *
  * \return The status code of the application. 0 for success, any other value
  *         for an error.
  *
  */
int
MainWindow::run()
{
  std::cout << "Hit a key to exit..." << std::endl;


  SDL_Color textColor = {255, 255, 255};
  SDL_Color greenColor = {55, 255, 55};

  texts.push_back(new Text(font, 10, 10, "FPS :", greenColor));
  texts.push_back(new Text(font, 10, 760, "<SPACE> to change rendering mode", greenColor));
  texts.push_back(new Text(font, 10, 760, "Any other key to exit", greenColor));

  fpsText = new Text(font, 70, 10, "---", textColor);

  
  // The window is open: could enter program loop here (see SDL_PollEvent())
  // Implement FPS:
  bool running = true;
  while (running) {

    Uint64 start = SDL_GetPerformanceCounter();

    // Do event loop
    // Do physics loop
    // Do rendering loop
    SDL_SetRenderDrawColor( windowRenderer, BG, 255 );
    SDL_RenderClear(windowRenderer);
    
    for (auto t : texts)
      t->draw(windowRenderer);

    // Draw fractal
    if (dirty)
      {
	drawers[actuelDrawerIndex]->draw(buffer);
	dirty = false;
      }
    /// Copy the buffer result to SDL
    auto tex = SDL_CreateTextureFromSurface(windowRenderer,
					    buffer->getSurface());
    SDL_RenderCopy(windowRenderer, tex, NULL, NULL);
    
    // FPS handling
    Uint64 end = SDL_GetPerformanceCounter();
    float elapsed = (end - start) / (float)SDL_GetPerformanceFrequency();
    fps_update_delta +=  elapsed * 100000;
    
    if (fps_update_delta > fps_update_ms)
      {
	auto fps= std::to_string(static_cast<int>(ceil(1.0f / elapsed)));
	fps_update_delta = 0;
	fpsText->setText(fps);
      }
    fpsText->draw(windowRenderer);
    
    SDL_RenderPresent(windowRenderer); // Finally Update the surface

	
    SDL_Event event;
    if (SDL_PollEvent(&event))
      {
        if (event.type == SDL_KEYDOWN )
	  {
	    if ( event.key.keysym.scancode == SDL_SCANCODE_SPACE)
	      {
		std::cout << "SPACE" << std::endl;
		// Change drawing backend
	      }
	    else
	      {
		printf( "? (0x%04X)\n", event.key.keysym.scancode);
		running=false;
	      }


	  }

      }
  }

  return 0;
}
