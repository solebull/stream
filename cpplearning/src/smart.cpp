// Smart pointer tests
#include <iostream>
#include <memory>
#include <vector>

using namespace std;


struct Obj {
  Obj() = default;
  Obj(const Obj&) = default;
  Obj(Obj&&) = default;
  Obj& operator=(const Obj&) = default;
  Obj& operator=(Obj&&) = default;
};

vector<shared_ptr<Obj>>
getObjs()
{
  vector<shared_ptr<Obj>> v;
  v.push_back(make_shared<Obj>());
  return v;
}

void
printObj(shared_ptr<Obj> o)
{
  cout << o.get() << endl;
}

void
printObjPtr(Obj* o)
{
  cout << o << endl;
}

int
main()
{
  /*
  auto o = getObjs();
  for (auto &b : o)
    cout << b.get() << endl;
  */

  auto o=make_shared<Obj>();
  
  printObj(o);

  
  //  printObj(o);
}
