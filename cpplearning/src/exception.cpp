#include "exception.hpp"

#include <iostream>
#include <sstream>

Exception::Exception(int errorCode, const std::string &message) noexcept:
  errorCode(errorCode), m_message(message)
{
  std::cout << this->errorCode << ": "<< this->m_message << std::endl;
}

Exception::Exception(int errorCode, const char *file, const char *function,
	  unsigned int line, const std::string &message) noexcept:
  errorCode(errorCode), m_message(message)
{
  std::ostringstream oss;
  oss << file << ":" << line << "->" << function << "(): " << message
      << std::endl;
  
  this->m_message = oss.str();
}
  
const char *Exception::what() const noexcept
{
  return this->m_message.c_str();
}
