

#include <iostream>
#include <list>
#include <memory>

using namespace std;

class Animal
{
public:
  virtual ~Animal() = default;
  
  void move()  {  std::cout << "Animal moves!" << std::endl;  }
  virtual void doThing()=0;

  Animal() = default;
  Animal(const Animal&) = default;
  Animal(Animal&&) = default;
  Animal& operator=(const Animal&) = default;
  Animal& operator=(Animal&&) = default;
  
protected:
  int nombreDePattes{};
};

class Cat : public Animal
{
public:
  void doThing() override { std::cout << "Meaoww!" << std::endl; }
};

class Dog : public Animal
{
public:
  void doThing() override { std::cout << "Waf waf!" << std::endl; }
};


/*
 */
int
main()
{
  /*  list<std::unique_ptr<Animal>> la;
  
  la.push_back(std::unique_ptr<Animal>(new Cat()));
  for (auto li : la)
    li->doThing();
  */
  
  return 0;
}
