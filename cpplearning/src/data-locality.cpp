/** From http://gameprogrammingpatterns.com/data-locality.html
  *
  * Also using http://gameprogrammingpatterns.com/component.html
  */

#include <vector>

class AIComponent
{
public:
  void update() { /* Work with and modify state... */ }

private:
  // Goals, mood, etc. ...
};

class PhysicsComponent
{
public:
  void update() { /* Work with and modify state... */ }

private:
  // Rigid body, velocity, mass, etc. ...
};

class RenderComponent
{
public:
  void render() { /* Work with and modify state... */ }

private:
  // Mesh, textures, shaders, etc. ...
};



class GameEntity
{
public:
  GameEntity(AIComponent* ai,
             PhysicsComponent* physics,
             RenderComponent* render)
  : ai_(ai), physics_(physics), render_(render)
  {}

  AIComponent* ai() { return ai_; }
  PhysicsComponent* physics() { return physics_; }
  RenderComponent* render() { return render_; }

private:
  AIComponent* ai_;
  PhysicsComponent* physics_;
  RenderComponent* render_;
};


int
main()
{
  using std::vector;
  
  bool gameOver=false;
  int maxFrame=1000;
  int nbframe=0;

  vector<GameEntity> entities;
  /*
  vector<AIComponent> vai;
  vector<PhysicsComponent> vph;
  vector<RenderComponent> vre;
   */
  while (!gameOver)
    {
      
      // Process AI.
      for (auto& e : entities)    e.ai()->update();
      for (auto& e : entities)	  e.physics()->update();
      for (auto& e : entities)	  e.render()->render();

      /*
      for (auto& e : vai)    e.update();
      for (auto& e : vph)	  e.update();
      for (auto& e : vre)	  e.->render();
      */
      
      // Other game loop machinery for timing...
      gameOver = nbframe++>maxFrame;
}
  return 0;
}

