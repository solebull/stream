#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <string>

// An utility header, no classes here, just public functions
// sometimes used by several classes

/** A macro used to check SDL commands return value
  *
  * MSG is the message if it failed (what the command tried to do)
  * CMD is the command that should return 0 on success
  *
  */
#define CSC(MSG, CMD) check_sdl_cmd(MSG, #CMD, __FILENAME__, __LINE__, CMD)

/** an utility struct used tp keep track opf the closest node
  *
  */
typedef struct {
  int    id;        //!< The id of the node
  double distance;  //!< The distance between the mouse and the node
} ClosestNode;

double distance(int x1, int y1, int x2, int y2);

void check_sdl_cmd(const std::string&, const std::string&,
		   const std::string&, int, int);

#endif // !__UTILS_HPP__
