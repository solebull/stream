#include <set>
#include <memory>

typedef int Widget;

using std::set;

/*Widget&
get_ref()
{
  
  Widget w;
  return w;   // segfault
  
}
*/

Widget*
get_ptr()
{
  // Try to return a shared ptr
  return new Widget();
}

auto
get_shared()
{
  // Try to return a shared ptr
  return std::make_shared<Widget>();
}

auto
get_unique()
{
  // Try to return a shared ptr
  return std::make_unique<Widget>();
}


set<Widget>
get_huge_load()
{
  set<Widget> ret = {6, 10, 5, 1};

  return ret;   // no deep copy
}


void
using_set(set<Widget>& s) // No deep copy
{
  ///
  //  for (auto& a : s)
    //
}

int
main()
{
  // auto r = get_ref();
  
  //  auto a = get_ptr(); // memory leak
  
   auto a = get_shared(); // No memory leak
  //  auto a = get_unique(); // No memory leak
  auto b = a;
  
  auto l = get_huge_load();
  return 0;
}
