#ifndef __LIST_TEST_H__
#define __LIST_TEST_H__

#include <check.h>
#include <stdio.h>

START_TEST(test_list_create_nonnull)
{
  int i = 5;
  List* l = list_create(&i);
  ck_assert_ptr_ne(l, NULL);
  ck_assert_ptr_ne(l->data, NULL);
  ck_assert_ptr_eq(l->next, NULL);
}
END_TEST

START_TEST(test_list_create_data)
{
  int i = 5;
  List* l = list_create(&i);
  int data = *(int*)l->data;
  ck_assert_int_eq(data, i);
}
END_TEST


START_TEST(test_list_len_0)
{
  int i = 5;
  List* l = list_create(&i);
  ck_assert_int_eq(list_len(l), 1);
}
END_TEST

START_TEST(test_list_append)
{
  int i = 5, j=12;
  List* l = list_create(&i);
  list_append(l, &j);
  ck_assert_int_eq(list_len(l), 2);
}
END_TEST

START_TEST(test_list_free)
{
  int i = 5, j=12;
  List* l = list_create(&i);
  list_free(&l);
  ck_assert_int_eq(list_len(l), 0);
  ck_assert_ptr_eq(l, NULL);
}
END_TEST


START_TEST(test_list_foreach)
{
  int i = 5, j=12;
  List* l = list_create(&i);
  list_append(l, &j);

  int sum=0;
  
  list_foreach(l, it)
    printf("- %d\n", *(int*)it->data);
      sum += *(int*)it->data;
  list_foreach_end(it)
    
    //  ck_assert_int_eq(sum, 17);


  list_free(&l);
}
END_TEST



Suite*
money_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("List");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_list_create_nonnull);
    tcase_add_test(tc_core, test_list_create_data);
    tcase_add_test(tc_core, test_list_len_0);
    tcase_add_test(tc_core, test_list_append);
    tcase_add_test(tc_core, test_list_free);
    tcase_add_test(tc_core, test_list_foreach);
    //    tcase_add_test(tc_core, );
    suite_add_tcase(s, tc_core);

    return s;
}

#endif // !__LIST_TEST_H__
