#ifndef __DRAWER_HPP__
#define __DRAWER_HPP__

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <string>

// Forward declarations
class Graph;
// End of forward declarations


/** The graph drawer class
  *
  * Must correctly draw nodes and edge in a gicen SDL renderer.
  *
  */
class Drawer
{
public:
  Drawer();
  void draw(SDL_Renderer*, Graph*);

  void setClosestNodeIndex(int);

  // Used to draw legend
  void drawNode(SDL_Renderer*, int, int);
  void drawClosestNode(SDL_Renderer*, int, int);
  void drawEdge(SDL_Renderer*, int, int,  int, int, int);

  void printKey(SDL_Renderer*, const std::string&);
  void printMod(SDL_Renderer*, const std::string&);
  
private:
  SDL_Color white; //!< The white (edges) drawing color
  TTF_Font* font;  //!< The edge weight drawing font

  TTF_Font* keyFont;  //!< Font used to print key/mod infos

  /// Shown as the closest from the mouse (-1 if none)
  int       closestNodeIndex; 
};

#endif //!__DRAWER_HPP__
