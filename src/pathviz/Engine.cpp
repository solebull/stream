#include "Engine.hpp"

#include <sstream>
#include <iostream>

#include "Text.hpp"
#include "Node.hpp"
#include "Drawer.hpp"


// window's background color
#define BG 0x20, 0x20, 0x20

#define MOUSE_CURSOR_LENGTH 25
#define MOUSE_CURSOR_WIDTH  3

#define FPS_INTERVAL 1.0 //seconds.

constexpr int window_width  = 1200;
constexpr int window_height = 800;

using namespace std;

// const string font_path="/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf";
//const string font_path="lazy.ttf";
const string font_path="../media/roboto.ttf";

/** Default constructor
  * 
  */
Engine::Engine():
  window(NULL),
  windowRenderer(NULL),
  font(NULL),
  
  width(window_width),
  height(window_height),
  title("pathviz : A pathfinder visualizer"),
  running(true),
  hwrenderer(false),
  previousTicks(0),
  
  closest{-1, 5000},
  tempMsg{"", false, 0, 1000, NULL},
  nodeNumbers(),
  countedFrames(0),
  fps{"--- FPS", 1000, 20000 /* +1000 */, 0, 0}
{
  textColor = {255, 255, 255};
  greenColor = {55, 255, 55};
}

/** Engine desctructor
  *
  */
Engine::~Engine()
{
  TTF_Quit();
  /*  if (window) // Segfault :(
    SDL_DestroyWindow( window );
  */
  
  //Quit SDL subsystems
  //  SDL_Quit();

  if (tempMsg.text)
    delete tempMsg.text;
  
}

/** Initialize SDL stuff
  *
  * No return value here. We massively use std::runtime_error throwing instead.
  *
  */
void
Engine::initSDL(void)
{
  //Initialize SDL
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    LT("SDL could not initialize! SDL_Error: " << SDL_GetError());

  // TTF  
  if(TTF_Init() < 0)
    LT("Erreur d'initialisation de TTF_Init : " << TTF_GetError());

  //Create window SDL_WINDOW_OPENGL|
  window = SDL_CreateWindow( title, 1600, 10, width, height,
			     SDL_WINDOW_SHOWN );
  if( !window )
    LT( "Window could not be created! SDL_Error: " <<  SDL_GetError());

  //    windowRenderer = SDL_GetRenderer(window);
  // Thanks to GhengysVIII
  windowRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if (windowRenderer)
    {
      L("Using hardware renderer");
      hwrenderer = true;
    }
  else
    {
      L("Can't use hardware renderer : " << SDL_GetError());
      L("Using software renderer");
      windowRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
      if (!windowRenderer)
	LT("Can't use HW or SF renderer : " << SDL_GetError());
    }
}


/** The application endless loop
  *
  * \return The application result. 0 for normal operations, !0 in case
  *         of errors. Should be used as main() function return.
  *
  */
int
Engine::run()
{
      
  SDL_ShowCursor(SDL_DISABLE);
  font = TTF_OpenFont(font_path.c_str(), 18);
  if( !font )
    LT( "Font '" << font_path <<  "' is NULL! SDL_Error: " <<  TTF_GetError())
      
  // Instructions/Keys+Actions
  int i=10;
  texts.push_back(new Text(font, width - 300, i+=30, "Actions:", greenColor));
  addInstructions(i+=20, "LMB", "Change starting node");
  addInstructions(i+=20, "RMB", "Forbid pointed node");
  addInstructions(i+=20, "mouse", "Change destination");
  addInstructions(i+=20, "-/+", "Change node number");
  addInstructions(i+=20, "r", "Randomize a new map");
  addInstructions(i+=20, "c", "Change cursor mode");
  addInstructions(i+=20, "l", "limit FPS ");
  i += 50;

  Drawer d;
  d.setClosestNodeIndex(1);

  // Legend texts : legends object are drawn later, while updating drawer
  int lx = width - 300;
  texts.push_back(new Text(font, lx, i+=30, "Legend:", greenColor));

  texts.push_back(new Text(font, lx+60, i+=20, "Node", greenColor));
  texts.push_back(new Text(font, lx+60, i+=20, "Selected Node", greenColor));
  texts.push_back(new Text(font, lx+60, i+=20, "Weighted edge", greenColor));
  
  // Used renderer info
  string rend;
  if (hwrenderer)
    rend = "Using hardware renderer";
  else
    rend = "Using software renderer";
  addTemporaryMessage(rend);
  
  // Finally the loop
  while(running)
    {
      SDL_Event event;
      auto ticks = SDL_GetTicks();
      Uint32 delta = ticks - previousTicks;
      //Clear background before drawing
      CSC ("Changing draw color",
	   SDL_SetRenderDrawColor( windowRenderer, BG, 255 ));
      CSC ("Clear renderer",
	   SDL_RenderClear(windowRenderer));
      
      //While there's an event to handle
      while( SDL_PollEvent( &event ) )
        {
	  closest = {-1, 5000};
	  computeClosestNode();
	  d.setClosestNodeIndex(closest.id);
	  
	  switch( event.type )
	    {
	      /* Keyboard event */
	      /* Pass the event data onto PrintKeyInfo() */
	    case SDL_KEYDOWN:
	      keyMod.injectKeyDown(event.key.keysym);
	      if (event.key.keysym.sym == SDLK_r)
		{
		  int r = g.randomize(nodeNumbers.number());
		  addTemporaryMessage("Graph randomized (" +
				      to_string(nodeNumbers.number()) +
				      " nodes " + to_string(r) + " rejected)");
		}
	      else if (event.key.keysym.sym == SDLK_KP_PLUS)
		{
		  nodeNumbers.next();
		  addTemporaryMessage("Switching up to " +
				      to_string(nodeNumbers.number()) +
				      " nodes");
		  g.randomize(nodeNumbers.number());
		}
	      else if (event.key.keysym.sym == SDLK_KP_MINUS)
		{
		  nodeNumbers.previous();
		  addTemporaryMessage("Switching down to " +
				      to_string(nodeNumbers.number()) +
				      " nodes");
		  g.randomize(nodeNumbers.number());
		}
	      else
		{
		  //	  keyMod.injectModUp(event.key.keysym.mod);
		  printKeyInfo( &event.key );
		}
	      break;
	    case SDL_KEYUP:
	      keyMod.injectKeyUp(event.key.keysym);
	      if (event.key.keysym.sym == SDLK_ESCAPE)
		running = false;
	      else
		printKeyInfo( &event.key );
	      break;
	    case SDL_QUIT:
	      running = false;
	    default:
	      
	      break;
	    }
	}
      
      d.draw(windowRenderer, &g);

      // Draw legend objects
      d.drawNode(windowRenderer, lx + 4 + 20, 300 - 8);
      d.drawClosestNode(windowRenderer, lx + 4 + 20, 320 - 8);
      d.drawEdge(windowRenderer, lx, 340 - 12, lx + 50, 340 - 12, 10);

      keyMod.draw(windowRenderer, &d);
      
      for (auto& t : texts)
	t->draw(windowRenderer);          // Draw some text

      printTemporaryMessage(delta);
      
      ++countedFrames;
      updateFps(delta);
      drawMouseCursor();                 // Draw cursor after text
      SDL_RenderPresent(windowRenderer); // Finally Update the surface

      previousTicks = ticks;
    }
  return 0;
}

/** Print all information about a key event 
  *
  * \param key The SDL key event.
  *
  */
void
Engine::printKeyInfo( SDL_KeyboardEvent *key )
{
  /* Is it a release or a press? */
  if( key->type == SDL_KEYUP )
    printf( "Release:- " );
  else
    printf( "Press:- " );
  
  /* Print the hardware scancode first */
  printf( "Scancode: 0x%02X", key->keysym.scancode );
  /* Print the name of the key */
  printf( ", Name: %s", SDL_GetKeyName( key->keysym.sym ) );
  /* We want to print the unicode info, but we need to make */
  /* sure its a press event first (remember, release events */
  /* don't have unicode info                                */
  if( key->type == SDL_KEYDOWN ){
    /* If the Unicode value is less than 0x80 then the    */
    /* unicode value can be used to get a printable       */
    /* representation of the key, using (char)unicode.    */
    printf(", Unicode: " );
    if( key->keysym.scancode < 0x80 && key->keysym.scancode > 0 ){
      printf( "%c (0x%04X)", (char)key->keysym.scancode,
	      key->keysym.scancode );
    }
    else{
      printf( "? (0x%04X)", key->keysym.scancode );
    }
  }
  printf( "\n" );
  /* Print modifier info */
  //  PrintModifiers( key->keysym.mod );
}

/** Print modifier info 
  *
  * \param mod The keyboard modifiers.
  *
  */
void
Engine::PrintModifiers( Uint16 mod )
{
  printf( "Modifers: " );
  
  /* If there are none then say so and return */
  if( mod == KMOD_NONE ){
    printf( "None\n" );
    return;
  }
  
  /* Check for the presence of each SDLMod value */
  /* This looks messy, but there really isn't    */
  /* a clearer way.                              */
  if( mod & KMOD_NUM ) printf( "NUMLOCK " );
  if( mod & KMOD_CAPS ) printf( "CAPSLOCK " );
  if( mod & KMOD_LCTRL ) printf( "LCTRL " );
  if( mod & KMOD_RCTRL ) printf( "RCTRL " );
  if( mod & KMOD_RSHIFT ) printf( "RSHIFT " );
  if( mod & KMOD_LSHIFT ) printf( "LSHIFT " );
  if( mod & KMOD_RALT ) printf( "RALT " );
  if( mod & KMOD_LALT ) printf( "LALT " );
  if( mod & KMOD_CTRL ) printf( "CTRL " );
  if( mod & KMOD_SHIFT ) printf( "SHIFT " );
  if( mod & KMOD_ALT ) printf( "ALT " );
  printf( "\n" );
}

/** Draw a custom mouse cursor, based on filled rectangles
  * 
  */
void
Engine::drawMouseCursor(void)
{
  int x, y;
  SDL_GetMouseState(&x, &y);

  // Horizontal rect
  SDL_Rect h;
  h.x = x - (MOUSE_CURSOR_LENGTH / 2);
  h.y = y - (MOUSE_CURSOR_WIDTH / 2);
  h.w = MOUSE_CURSOR_LENGTH;
  h.h = MOUSE_CURSOR_WIDTH;

  SDL_Rect v;
  v.x = x - (MOUSE_CURSOR_WIDTH / 2);
  v.y = y - (MOUSE_CURSOR_LENGTH / 2);
  v.w = MOUSE_CURSOR_WIDTH;
  v.h = MOUSE_CURSOR_LENGTH;

  SDL_SetRenderDrawColor(windowRenderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
  SDL_RenderFillRect(windowRenderer, &h);
  SDL_RenderFillRect(windowRenderer, &v);
}

/** Add both text objects, representing user's instructions
  *
  * \param y    The vertical offset. All are printed at the same X value.
  * \param key  The key/manipulation described.
  * \param info What this key should do.
  *
  */
void
Engine::addInstructions(int y, const string& key, const string& info)
{
  int x = width - 300;
  texts.push_back(new Text(font, x, y, key, textColor));
  texts.push_back(new Text(font, x+60, y, info, greenColor));
}

/** Add a temporary printed message
  *
  *
  *
  */
void
Engine::addTemporaryMessage(const std::string& msg)
{
  tempMsg.message = msg;
  tempMsg.remaining_time = tempMsg.total_time;
  tempMsg.show = true;
  tempMsg.text = new Text(font, 20, window_height - 30, msg, textColor);
}

// Called for each frame
void
Engine::printTemporaryMessage(Uint32 delta)
{
  if (!tempMsg.show)
    return;

  tempMsg.remaining_time -= delta;
  if (tempMsg.remaining_time < 0)
    tempMsg.show = false;

  // print
  if (tempMsg.text)
      tempMsg.text->draw(windowRenderer);
}

/** Finally set the closest node
 *
 */
void
Engine::computeClosestNode(void)
{
  int x, y;
  SDL_GetMouseState(&x, &y);

  auto nodes = g.getNodes();
  int idx = 0;
  for (auto& n : nodes)
    {
      auto dist = distance(x, y, n->x, n->y);
      if (dist < closest.distance)
	{
	  closest.id = idx;
	  closest.distance = dist;
	}
      ++idx;
    }
}

/** Called for each frame, only print if necessary
  *
  * \param ticks are 
  *
  */
void
Engine::updateFps(Uint32 delta)
{
  // Compute FPS
  fps.frames++;
  if (fps.lasttime < SDL_GetTicks() - FPS_INTERVAL*1000)
    {
      fps.lasttime = SDL_GetTicks();
      fps.current_fps = to_string(fps.frames) + " FPS";
      fps.frames = 0;
   }

  // Only prints
  fps.last_update_delta += delta;
  if (fps.last_update_delta > fps.update_delta)
    {
      if (fps.text)
	delete fps.text;

      fps.last_update_delta = 0;
      fps.text = new Text(font, window_width - 100, window_height - 30,
			  fps.current_fps, textColor);
    }

  // print
  if (fps.text)
    fps.text->draw(windowRenderer);
}

