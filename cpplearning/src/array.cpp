#include <iostream>
#include <vector>
#include <memory>

using namespace std;

struct Widget
{
  int i;
  Widget(){ cout << "In ctor" << endl; }
  ~Widget(){ cout << "In dtor" << endl; }
};

struct Container
{
  shared_ptr<Widget> i;
  Container()
  {
    i=std::make_shared<Widget>();
  }
};

int
main()
{

  /*
  using std::vector;
  using std::unique_ptr;
  using std::make_unique;
  */

  for (int i=0; i<1000; ++i)
    {
      /*
      vector<unique_ptr<Widget>> arr;
      arr.push_back(make_unique<Widget>());
      arr.push_back(make_unique<Widget>());
      arr.push_back(make_unique<Widget>());
      */
      
  Widget a,b,c;
  vector<Widget> arr = {a,b,c};
    }
  return 0;
}
