#include "Text.hpp"

using namespace std;

/** Text constructor
  *
  * \param vFont  The TTF font object to be used
  * \param vX     The X position
  * \param vY     The Y position
  * \param vText  The text to be drawn
  * \param vColor The Font color
  *
  */
Text::Text(TTF_Font* vFont, int vX, int vY,
	   const std::string& vText, SDL_Color vColor):
  font(vFont),
  texture(NULL),
  text(vText),
  color(vColor)
{
  // First, place the rect, its size will be computed later
  rect.x = vX;
  rect.y = vY;
}

/** The destructor
  * 
  */
Text::~Text()
{
  // Do not delete font here, it can be use later
  SDL_DestroyTexture(texture);
}

/** Draw the constructed text to the renderer
  *
  * \param renderer basically the windowRenderer
  *
  */
void
Text::draw(SDL_Renderer* renderer)
{
  if (!texture)
    createTexture(renderer);

  
  SDL_RenderCopy(renderer, texture, NULL, &rect);
}

/** Create the text texture for this renderer
  *
  * We speed up the drawing by creating this texture only once.
  *
  * \param renderer basically the windowRenderer
  *
  */
void
Text::createTexture(SDL_Renderer* renderer)
{
  SDL_Surface* surface = TTF_RenderUTF8_Blended(font, text.c_str(), color);
  texture = SDL_CreateTextureFromSurface(renderer, surface);
  rect.w = surface->w;
  rect.h = surface->h;
  SDL_FreeSurface(surface);
}
