#include "Buffer.hpp"


#include <gtest/gtest.h>

constexpr int argc=1;
constexpr char** argv=nullptr;


/*TEST(Buffer, width) {
  Buffer b(100, 200);
  ASSERT_EQ(b.getWidth(), 100);
}

TEST(Buffer, height) {
  Buffer b(100, 200);
  ASSERT_EQ(b.getHeight(), 200);
}

*/
TEST(Buffer, windowrenderer_nullptr)
{
  EXPECT_THROW(new Buffer(argc, argv, NULL, 100, 200), std::runtime_error);

}
