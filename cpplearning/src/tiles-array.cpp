#include <vector>
#include <list>
#include <iostream>
#include <ctime>
#include <memory>

using namespace std;

constexpr int NB=1000;

unsigned int tileid=1;
unsigned int entityid=1;

/// Compute Updates per second
struct UPS
{
  void start() {
    current_ticks = clock();
  }

  // number: The number of element updated between start and end
  void end(int number) {
    auto delta_ticks = clock() - current_ticks;
    clock_t fps;
    if(delta_ticks > 0)
      fps = CLOCKS_PER_SEC / delta_ticks;
    cout << " => "<< fps/number << " UPS" <<  endl;
  }
  clock_t current_ticks;
};

struct Tile
{
  Tile():id(tileid++){
    //    cout << "  In Tile #" << id << " ctor" << endl;
  }

  void update() {
    //    cout << ".";
  }
  
  unsigned int id;
};

struct Entity
{
  Entity():id(entityid++){
    //    cout << "  In Entity #" << id << " ctor" << endl;
  }

  void update() {
    //    cout << ".";
  }
  
  unsigned int id;
};


struct GameEngine
{
public:
  void loadTiles()
  {
    cout << "Loading " << NB << " tiles... " << endl;
    for (int i=0; i<NB; ++i)
      tiles.push_back(Tile());
  }

  void loadEntities()
  {
    cout << "Loading " << NB << " entities... " << endl;
    for (int i=0; i<NB; ++i)
      entities.push_back(make_shared<Entity>());
  }
  
  void update() {
    cout << "Updating tiles..." << endl << "  ";
    ups.start();
    for (auto& t : tiles)
      t.update();
    ups.end(tiles.size());

    cout << "Updating entities..." << endl << "  ";
    ups.start();
    for (auto& t : entities)
      t->update();
    ups.end(entities.size());

  }
  
private:
  vector<Tile> tiles;
  list<shared_ptr<Entity>> entities;
  UPS ups;
};

int
main()
{
  GameEngine ge;

  ge.loadTiles();
  ge.loadEntities();

  ge.update();
}
