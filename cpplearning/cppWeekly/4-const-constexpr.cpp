struct Type
{
  //static void do_thing() {}
  constexpr int do_thing() const { return data + 4; }
  constexpr int do_other_thing() const { return data + 6; }

  static int data{};
};

int
main(int /* argc */, const char** /* argv */)
{
  Type obj;
  obj.date;
  Type::data
  if constexpr (sizeof(long) > 4)
    {
      obj.do_thing();
    }
  else
    {
      obj.do_other_thing();
    }
  return 0;
}

   
