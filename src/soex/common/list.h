#ifndef _LIST_H_
#define _LIST_H_

#include <stdlib.h> // USES size_t

typedef struct _List
{
  void*         data;
  struct _List* next;
} List;

#define list_foreach(LIST, ITER) List* ITER = LIST; \
  while (ITER) { 				    \
    
#define list_foreach_end(ITER) ITER = ITER->next; }

List*  list_create(void* data);
void   list_append(List* l, void *data);
size_t list_len(List *l);
void   list_free(List **l);


#endif // !_LIST_H_
