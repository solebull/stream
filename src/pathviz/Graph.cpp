#include "Graph.hpp"

#include <cmath>

#include "Node.hpp"
#include "Utils.hpp"   // USES distance()
#include "Log.hpp"     // USES L()

using namespace std;

/** Constructor for a randomized graph
  *
  */
Graph::Graph():
  minimalDistance(20)  // TODO: Arbitrary value, shoud be based on drawer
{
  randomize();
}

/** Randomize this graph
  *
  * \param size New size in number of nodes.
  *
  * \return Number of rejected nodes
  *
  */
int
Graph::randomize(int size)
{
  nodes.clear();
  edges.clear();
  int rejected = 0;
  for (int i=0; i< size; ++i)
    {
      int distance = 0;
      while (nodes.size() < size)
	{
	  Node* newone = new Node();
	  distance = computeClosestNode(newone->x, newone->y);
	  if (distance >= minimalDistance || distance == -1 )
	    {
	      L("Accepeted node (distance " << distance << ")");
	      nodes.push_back(newone);
	    }
	  else
	    {
	      L("Rejecting node (distance " << distance << ")");
	      rejected++;
	    }
	}
    }


  // Compute egdes
  auto e = new Edge();
  e->a = 0;
  e->b = 1;
  e->weight = (int)distance(nodes[e->a]->x, nodes[e->a]->y,
			    nodes[e->b]->x, nodes[e->b]->y);
  edges.push_back(e);
  
  //  addEdge(0, 1);  // Currently not safe due to segfaults
  
  return rejected;
}

/** Try to add an edge from two nodes index
  *
  * Only add if no existing edge intersect
  *
  * \return true if edge is added, false if it intersects with an existing one.
  *
  */
bool
Graph::addEdge(int a, int b)
{
  if (!intersects(a, b))
    {
      auto e = new Edge();
      e->a = a;
      e->b = b;
      e->weight = (int)distance(nodes[e->a]->x, nodes[e->a]->y,
				nodes[e->b]->x, nodes[e->b]->y);
      edges.push_back(e);
      return true;
    }
  else
    return false;
}



/** Get all nodes
  *
  * \return The Node vector.
  *
  */
const std::vector<Node*>&
Graph::getNodes() const
{
  return nodes;
}

/** Get all edges
  *
  * \return The Edge vector.
  *
  */
const std::vector<Edge*>&
Graph::getEdges() const
{
  return edges;
}


/** Compute closes node distance from an arbitrary point
  *
  * /param The point x position
  * /param The point y position
  *
  * \return The distance or -1 if no node to compare to.
  *
  */
int
Graph::computeClosestNode(int x, int y) const
{
  int max = 100000000;
  ClosestNode closest;
  closest.distance = max;
  int idx = 0;
  for (auto& n : nodes)
    {
      auto dist = distance(x, y, n->x, n->y);
      L("distance: " << dist);
      if (dist < closest.distance)
	{
	  closest.id = idx;
	  closest.distance = dist;
	}
      ++idx;
    }

  if (closest.distance == max)
    return -1;
  
  return closest.distance;
}

/** Add a new node with given coordinates.
  *
  * \param x X position
  * \param x Y position.
  *
  */
void
Graph::addNode(int x, int y)
{
  auto n = new Node();
  n->x = x;
  n->y = y;
  nodes.push_back(n);
}


// Given three colinear points p, q, r, the function checks if
// point q lies on line segment 'pr'
bool onSegment(const Node& p, const Node& q, const Node& r)
{
    if (q.x <= max(p.x, r.x) && q.x >= min(p.x, r.x) &&
        q.y <= max(p.y, r.y) && q.y >= min(p.y, r.y))
       return true;
 
    return false;
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int
orientation(const Node& p, const Node& q, const Node& r)
{
    // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
    // for details of below formula.
    int val = (q.y - p.y) * (r.x - q.x) -
              (q.x - p.x) * (r.y - q.y);
 
    if (val == 0) return 0;  // colinear
 
    return (val > 0)? 1: 2; // clock or counterclock wise
}

// The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
bool
doIntersect(const Node& p1, const Node& q1,
	    const Node& p2, const Node& q2)
{
    // Find the four orientations needed for general and
    // special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);
 
    // General case
    if (o1 != o2 && o3 != o4)
        return true;
 
    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;
 
    // p1, q1 and q2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;
 
    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;
 
     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;
 
    return false; // Doesn't fall in any of the above cases
}

// Params are Edges indexes
// Mainly from geeksforgeeks.org/check-if-two-given-line-segments-intersect/
bool
Graph::intersects(int a, int b)
{
  if (a<edges.size() && b<edges.size())
    {
      return doIntersect(*nodes[edges[a]->a],
			 *nodes[edges[a]->b],
			 *nodes[edges[b]->a],
			 *nodes[edges[b]->b]);
    }
  else
    return true;
}

/** Returns true if the given edge index is valid 
  *
  * \param The edge id.
  *
  */
bool
Graph::isEdgeValid(int id)
{
  return id<edges.size();
}
