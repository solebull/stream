#include "Graph.hpp"

#include <gtest/gtest.h>

class TestableGraph : public Graph
{
public:
  bool _intersects(int a, int b) {    return intersects(a, b);  }
  void _addNode(int x, int y)    {    addNode(x, y);            }
  bool _addEdge(int a, int b)    {    return addEdge(a, b);     }
  bool _isEdgeValid(int x)       {    return isEdgeValid(x);    }
};

// addNode really add a node
TEST( Graph, addNode )
{
  TestableGraph tg;
  tg.randomize(0);
  ASSERT_EQ(  tg.getNodes().size(), 0);
  tg._addNode(10, 10);
  ASSERT_EQ(  tg.getNodes().size(), 1);
}

TEST( Graph, intersects )
{
  TestableGraph tg;
  tg.randomize(0);

  tg._addNode(10, 10);
  tg._addNode(30, 10);
  tg._addNode(20, 0);
  tg._addNode(20, 20);

  tg._addEdge(0, 2);
  tg._addEdge(1, 3);
  
  ASSERT_EQ( tg._intersects(0, 1), true);
}

TEST( Graph, intersects_false )
{
  TestableGraph tg;
  tg.randomize(0);

  tg._addNode(10, 10);
  tg._addNode(30, 10);
  tg._addNode(10, 20);
  tg._addNode(30, 20);

  tg._addEdge(0, 1);
  tg._addEdge(2, 3);

  ASSERT_EQ( tg._intersects(0, 1), false);
}

/** Test if edges IDs are valid */
TEST( Graph, isEdgeValid_1 )
{
  TestableGraph tg;
  tg.randomize(0);

  tg._addNode(30, 10);
  tg._addNode(10, 20);

  
  
  ASSERT_EQ( tg._isEdgeValid(0), true);
  ASSERT_EQ( tg._isEdgeValid(2), false); // false : 2 is out of bound
}

TEST( Graph, intersects_outOfBound )
{
  TestableGraph tg;
  tg.randomize(0);

  /*  tg._addNode(10, 10);
  tg._addNode(30, 10);
  tg._addNode(10, 20);
  tg._addNode(30, 20);
  */
  // Shoudln't fire a segfault
  //  ASSERT_EQ( tg._intersects(0, 1), false);
}
