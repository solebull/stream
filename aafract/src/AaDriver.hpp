#ifndef __AA_DRIVER_HPP__
#define __AA_DRIVER_HPP__

#include <aalib.h>

aa_driver* create_aafract_aadriver();
void       destroy_aafract_aadriver(aa_driver*);


#endif // !__AA_DRIVER_HPP__
