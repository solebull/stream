#ifndef __BUFFER_HPP__
#define __BUFFER_HPP__

#include <SDL2/SDL.h>
#include <aalib.h>
#include <string>

struct Color
{
  unsigned int r,g,b;

  // toSDLcolor()
  // roaaPalette()
};


/** A simple multi-backend pixelbuffer
  *
  * The goal would have to be a FractalDrawar subclass working on a pointer
  * to an instance of this class to draw, pixel by pixel, a complete fractal.
  *
  */
class Buffer
{
  /*  void setPixel(int x, int y, Color c)... */
public:
  Buffer(int, char**, SDL_Renderer*, int, int);
  ~Buffer();
  
  int getWidth()const;
  int getHeight()const;

  void setPixel(int, int, Color);
  void endFrame();
  SDL_Surface* getSurface();
  
protected:
  void setPixelSdl(SDL_Surface *surface, int x, int y, Color);
  void listAaDrivers();
  void listAaFonts();
  void printFontName();
  const aa_driver* getAaDriver(const std::string& shortname);
  
private:
  int width;    //!< Width in pixel of the final result
  int height;   //!< Height in pixel of the final result

  // SDL renderer
  SDL_Renderer* windowRenderer;
  SDL_Surface *surface;
  SDL_Texture *texture;
  
  // aalib context
  aa_context *context;
  int palette[256];
};

#endif // !__BUFFER_HPP__
