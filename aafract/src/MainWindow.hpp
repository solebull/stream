#ifndef __MAIN_WINDOW_HPP__
#define __MAIN_WINDOW_HPP__

#include <list>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// Forward declarations
class Buffer;
class Text;
class FractalDrawer;
// End of forward declarations

/** The main (and only) window opened by this application
  *
  */
class MainWindow
{
public:
  MainWindow(int, char**);
  ~MainWindow();

  int run();

private:
  SDL_Window*   window;   //!< Declare a pointer to the to-be-created SDL win
  SDL_Renderer* windowRenderer;  //!< The full window renderer
  TTF_Font*     font;            //!< The global TTF font

  int           fps_update_ms;    //!< Update text once every xx ms
  float         fps_update_delta; //!< Update if delta > ms
  
  Text*   fpsText;   //!< Not in texts list because often updated
  Buffer* buffer;    //!< Temporary, multi-backend, pixel buffer we draw on

  std::list<Text*>          texts;   // List of 'all' drawn TTF texts
  std::vector<FractalDrawer*> drawers; // All available fractal drawers
  int actuelDrawerIndex{}; //!< Index of the actual one in vector

  bool dirty;             //!< Current image must be redrawn
};

#endif // !__MAIN_WINDOW_HPP__
