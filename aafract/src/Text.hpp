#ifndef __TEXT_HPP__
#define __TEXT_HPP__

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

/** A simple SDL12_TTF-base dtext element
  *
  *
  *
  */
class Text
{
public:
  Text(TTF_Font*, int, int, const std::string&, SDL_Color);
  ~Text();

  void draw(SDL_Renderer*);
  void setText(const std::string&);
  
protected:
  void createTexture(SDL_Renderer*);
  
private:
  // Pointers
  TTF_Font*    font;    //!< The font used to draw text
  SDL_Texture* texture; //!< The underlying to be drawn texture

  // Vars
  std::string  text;    //!< Th text to be drawn
  SDL_Rect     rect;    //!< Position
  SDL_Color    color;   //!< Text color
};
#endif // !__TEXT_HPP__
