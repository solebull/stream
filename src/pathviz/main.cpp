//Using SDL and standard IO
#include <stdio.h>

#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <stdexcept>

#include "Engine.hpp"

/** The application main function
  *
  * \param argc Number of command line arguments (including app name).
  * \param args The command line arguments string array.
  *
  * \return The application result. 0 for normal operations, 
  *         !0 in case of errors. 
  *
  */
int main( int argc, char* args[] )
{
  srand (time(NULL));

  Engine e;
  try
    {
      e.initSDL();
    }
  catch (std::exception& e)
    {
      std::cerr << "Can't initialize SDL : " << SDL_GetError() << std::endl;
      return 1;
    }
  return e.run();;
}
