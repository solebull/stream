# aafract

An SDL2 application to show multiple fractale both as colored image and 
grayscale ascii art.

# Anatomy

## FractalDrawer

\startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
\enduml
