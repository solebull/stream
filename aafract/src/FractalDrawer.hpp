#ifndef __FRACTAL_DRAWER_HPP__
#define __FRACTAL_DRAWER_HPP__

#include "Buffer.hpp"

/** A base class for amm fractal drawer
  *
  * Subclasses must override draw() pure virtual function and directly draw
  * in the Buffer class.
  *
  */
class FractalDrawer
{
public:
  virtual ~FractalDrawer() = default;

  virtual void draw(Buffer*)=0;

  FractalDrawer() = default;
  FractalDrawer(const FractalDrawer&) = default;
  FractalDrawer(FractalDrawer&&) = default;
  FractalDrawer& operator=(const FractalDrawer&) = default;
  FractalDrawer& operator=(FractalDrawer&&) = default;

};

#endif // !__FRACTAL_DRAWER_HPP__
