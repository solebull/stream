#include <vector>
#include <iostream>

//#include <algorithm>  //operates on ranges
#include <numeric>    // USES accumulate


int
sum(const std::vector<float> &vec)
{
  return std::accumulate(begin(vec), end(vec), 0);
}


int
main()
{

  std::vector<float> v{5.2, 2.1, .23, .25};
  std::cout << sum(v) << std::endl;
  return 0;
}
