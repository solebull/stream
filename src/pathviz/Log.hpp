#ifndef __LOG_HPP__

#include <iostream>
#include <sstream>

// Simplest logger macro ever
#define L(args) \
  std::cout << __FILENAME__ << ':' << __LINE__ << " > " \
  << args << std::endl;

// To be chained with a throw command
#define LT(args) {	     \
    std::ostringstream oss;					\
  oss << __FILENAME__ << ':' << __LINE__ << " > " << args;	\
  std::cout << oss.str() << std::endl; \
  throw std::runtime_error(oss.str()); }


#define __LOG_HPP__
#endif // ! __LOG_HPP__
