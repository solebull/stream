#ifndef _FD_MANDELBROT_HPP_
#define _FD_MANDELBROT_HPP_

#include "FractalDrawer.hpp"

class fdMandelbrot : public FractalDrawer
{
public:
  void draw(Buffer*) override;
};


#endif // !_FD_MANDELBROT_HPP_
