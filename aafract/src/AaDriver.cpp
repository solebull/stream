#include "AaDriver.hpp"

#include <iostream>

/* 0 if failed, 1 if driver correctly initialized */
int
aadriver_init(const struct aa_hardware_params* hdparams, const void* userdata,
	      struct aa_hardware_params*,void**)
{
  std::cout << "AaDriver initialization" << std::endl;
  
  return 0;
}

void
aadriver_uninit(struct aa_context*)
{

}


void
aadriver_getsize (struct aa_context*, int* x, int* y)
{

}

void
aadriver_setattr (struct aa_context*, int)
{

}

void
aadriver_print (struct aa_context*, const char*)
{

}

void
aadriver_gotoxy (struct aa_context*, int, int)
{

}

void
aadriver_flush (struct aa_context*)
{

}

void
aadriver_cursormode (struct aa_context*, int)
{

}


aa_driver*
create_aafract_aadriver()
{
  aa_driver* aa=new aa_driver();
  aa->shortname = "aafract";
  aa->name = "aafract custom aalib driver";

  aa->init       = &aadriver_init;
  aa->uninit     = &aadriver_uninit;
  aa->getsize    = &aadriver_getsize;
  aa->setattr    = &aadriver_setattr;
  aa->print      = &aadriver_print;
  aa->gotoxy     = &aadriver_gotoxy;
  aa->flush      = &aadriver_flush;
  aa->cursormode = &aadriver_cursormode;
  return aa;
}

void
destroy_aafract_aadriver(aa_driver* aa)
{
  delete aa;
}
