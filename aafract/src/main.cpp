/** \file main.cpp
  * Main entry of the aafract application code base.
  *
  */

#include <iostream>

#include "MainWindow.hpp"

/*
#include <aalib.h>
aa_context *context;

...

  std::cout << "aafract :" << std::endl;

  context = aa_autoinit(&aa_defparams);
  if(context == NULL) {
    fprintf(stderr,"Cannot initialize AA-lib. Sorry\n");
    exit(1);
  }
  //  ...
  aa_close(context);
*/


/** Print a very helpfull message and exit
  *
  * \param progname The name of the program as seen in argv.
  *
  */
int
usage(const char* progname)
{
  using std::cout;
  using std::endl;

  cout << progname << " - A graphical aalib fractal viewer." << endl;
  cout << endl;
  cout << "Usage:" << endl
       << "  --help      " << "Show this usage message and exit." << endl;
  return 0;
}


/** Main entry of the program
  *
  * \return Static code of the program execution.
  *
  */
int
main(int argc, char** argv)
{

  if (argc > 1)
    return usage(argv[0]);
  
  MainWindow mw(argc, argv);
  return mw.run();
}
