#include "Buffer.hpp"

#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <bitset>

#include "AaDriver.hpp"

/** Default sized constructor
  *
  * \param argc Number of argument from command line.
  * \param argc Argument strings from command line.
  * \param wr   The currzent SDL's window renderer.
  *
  * \param w    The buffer's width in pixel.
  * \param h    The buffer's height in pixel.
  *
  */
Buffer::Buffer(int argc , char** argv, SDL_Renderer* wr, int w, int h):
  width(w),
  height(h),
  windowRenderer(wr),
  surface(nullptr),
  texture(nullptr)
{
  surface = SDL_CreateRGBSurface(0, width, height, 32, 0, 0, 0, 0);

  if (wr==nullptr)
    throw std::runtime_error("WindowRenderer pointer can't be NULL");

  // AAlib
  /// https://linux.die.net/man/3/aa_parseoptions
  aa_hardware_params* hpar = new aa_hardware_params;
  // bright, contrast, gamma, dither, inversion, randomval;
  aa_renderparams*    rpar = new aa_renderparams();
  
  if (aa_parseoptions(hpar, rpar, &argc, argv) != 1)
      throw std::runtime_error("aalib options parsing error");

  //  listAaDrivers();
  listAaFonts();
  //  printFontName();
  
  aa_driver* aad = create_aafract_aadriver();
  //  context = aa_init(getAaDriver("curses"), &aa_defparams /*hpar */, NULL);
  context = aa_init(aad, &aa_defparams /*hpar */, NULL);
  if(context == nullptr)
    throw std::runtime_error("Cannot initialize AA-lib. Sorry");

  aa_setpalette(palette, 0, 255, 255, 255);

  destroy_aafract_aadriver(aad);
}

Buffer::~Buffer()
{
  SDL_FreeSurface(surface);
  aa_close(context);
}

/** Return the  the buffer's width in pixel.
  *
  * \return  The buffer's width in pixel.
  *
  */
int
Buffer::getWidth()const
{
  return width;
}

/** Return the  the buffer's height in pixel.
  *
  * \return  The buffer's height in pixel.
  *
  */
int
Buffer::getHeight()const
{
  return height;
}

void
Buffer::setPixel(int x, int y, Color c)
{
  setPixelSdl(surface, x, y, c);
  aa_putpixel(context,x,y, 0);
}

void
Buffer::setPixelSdl(SDL_Surface *surface, int x, int y, Color col)
{
  auto color = 0xFFFFFF; 
  int bpp = surface->format->BytesPerPixel;
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
  
  if(bpp == 1)
    *p = color;
  else if(bpp == 2)
    *(Uint16*)p = color;
  else if(bpp == 4)
    *(Uint32*)p = color;
  else
    {
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
        {
            p[0] = (color >> 16) & 0xff;
            p[1] = (color >> 8) & 0xff;
            p[2] = color & 0xff;
        }
        else
        {
            p[0] = color & 0xff;
            p[1] = (color >> 8) & 0xff;
            p[2] = (color >> 16) & 0xff;
        }
    }
}

SDL_Surface*
Buffer::getSurface()
{
  return surface;
}

/** Must be call at the end of the drawing frame 
  *
  */
void
Buffer::endFrame()
{
  texture = SDL_CreateTextureFromSurface(windowRenderer, surface);
  aa_fastrender(context, 0, 0, aa_scrwidth(context), aa_scrheight(context));
}


/** List available aalib drivers in console
  *
  * The list comes from the \b aa_drivers NULL-terminated list provided by
  * aalib itself.
  *
  */
void
Buffer::listAaDrivers()
{
  using std::endl;

  std::ostringstream oss;
  // List aa drivers
  int driver_idx=0;
  
  auto driver = aa_drivers[driver_idx];
  while (driver != nullptr)
    {
      oss << "  " << driver->shortname << ' ' << driver->name << endl;
      driver = aa_drivers[++driver_idx];
    }

  std::cout << "Found " << driver_idx-1 << " aalib drivers :" << endl;
  std::cout << oss.str();
}

/** Return a listed AA driver from its shortname
  *
  *
  *
  */
const aa_driver*
Buffer::getAaDriver(const std::string& shortname)
{
  
  int driver_idx=0;
  
  auto driver = aa_drivers[driver_idx];
  while (driver != nullptr)
    {
      if (shortname == driver->shortname)
	{
	  std::cout << "Buffer::getAaDriver returning " <<
	    aa_drivers[driver_idx]->shortname <<  std::endl;
	  return aa_drivers[driver_idx];
	}

      driver = aa_drivers[++driver_idx];
    }

  throw std::runtime_error("Can't find AA driver named '" + shortname + "'");
}

/** Print the name of the default allib font found in the aa_hardware_params
  * struct
  *
  * Do not use it in normal usage. 
  *
  */
void
Buffer::printFontName()
{
  using std::cout;
  using std::endl;

  
  auto ctx = aa_autoinit(&aa_defparams);
  // see https://github.com/alexmac/alcextra/blob/master/aalib-1.4.0/src/aalib.h#L119
  if (ctx)
    {
      cout << ctx->params.font->name
	   << " (" << ctx->params.font->shortname
	   << ")" << endl;

      cout << ctx->driverparams.font->name
	   << " (" << ctx->driverparams.font->shortname
	   << ")" << endl;

    }
  else
    {
      cout << "Can't create a aalib context" << endl;
    }
}

/** Try to print all aalib-available fonts
  *
  *
  *
  */
void
Buffer::listAaFonts()
{
  using std::cout;
  using std::endl;

  std::ostringstream oss;
  // List aa fonts
  int font_idx=0;
  
  auto font = aa_fonts[font_idx];
  while (font != nullptr)
    {
      oss << "  " << font->shortname << ' ' << font->name << endl;
      font = aa_fonts[++font_idx];
      // There is also unsigned char *data;
    }

  // Defines here https://github.com/alexmac/alcextra/blob/master/aalib-1.4.0/src/font8.c
  cout << "Printing '" << aa_fonts[0]->name << "' :" << endl;
  for (int i=0; i<8; ++i)
    {
      for (int j=0; j<8; ++j)
	{
	  /*	  cout << std::setfill('0') << std::setw(3)
	       << (int)aa_fonts[0]->data[(i*j)+j] << " ";
	  */
	  std::bitset<8> x((int)aa_fonts[0]->data[(i*j)+i]);
	  std::cout << x << endl;
	}
      cout  << endl;
    }
  
  cout << "Found " << font_idx-1 << " aalib fonts :" << endl;
  cout << oss.str();
}
