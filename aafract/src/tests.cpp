#include <gtest/gtest.h>
//#include <iostream>

// Google Test test cases are created using a C++ preprocessor macro
// Here, a "test suite" name and a specific "test name" are provided.
TEST(module_name, test_name) {
    // Google Test will also provide macros for assertions.
    ASSERT_EQ(1+1, 2);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
