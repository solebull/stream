
#include <gtest/gtest.h>

int main(int ac, char* av[])
{
  // Logger instanciation
  testing::InitGoogleTest(&ac, av);
  return RUN_ALL_TESTS();
}
