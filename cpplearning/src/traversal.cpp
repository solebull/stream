// Traversing array/vector

#include <array>
#include <vector>
#include <iostream>
#include <ranges>     // C++20 only


int
main()
{
  using std::cout;
  using std::endl;
  
  // Array has a limited size
    std::array<int, 6> ai = { 1, 2, 3, 4, 5, 6};
  //std::vector<int> ai = { 1, 2, 3, 4, 5, 6, 8, 20, 346,  952, 1212, 436};

  cout << "ai : >>" << endl;

  /*  for (std::vector<int>::iterator i=ai.begin(); i!= ai.end(); ++i)
    cout << "  " << *i << ' ';
  */
  
  for (const auto& i:ai)
    cout << "  " << i << ' ';
  cout << endl;

  /* for (std::vector<int>::reverse_iterator i=ai.rbegin(); i!= ai.rend(); ++i)
    cout << "  " << *i << ' ';
  */
  cout << "ai : >>" << endl;
    for (const auto& i:ai | std::views::reverse)
      cout << "  " << i << ' ';
  
  cout << endl;;
  
  
  return 0;
}
