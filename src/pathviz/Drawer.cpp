#include "Drawer.hpp"

#include <SDL2/SDL.h>
#include "SDL2/SDL2_gfxPrimitives.h"
#include <string>

#include "Graph.hpp"
#include "Node.hpp"
#include "Text.hpp"
#include "Log.hpp"
#include "Utils.hpp" // USES CSC()

using namespace std;

constexpr int circles_radius = 8;

#define FNAME "../media/lazy.ttf"
#define KNAME "../media/roboto.ttf"

Drawer::Drawer():
  font(NULL),
  keyFont(NULL),
  closestNodeIndex(-1)
{
  white = {250, 250, 250};
  
  font = TTF_OpenFont(FNAME, 14);
  keyFont = TTF_OpenFont(KNAME, 18);
  if (!font)
    {
      L( "Font '" << FNAME <<  "' is NULL! SDL_Error: " <<  TTF_GetError());
    }
}

void
Drawer::draw(SDL_Renderer* r, Graph* g)
{
  // Nodes
  auto nodes = g->getNodes();
  int idx = 0;
  for (auto& n : nodes)
    {
      if (idx == closestNodeIndex)
	drawClosestNode(r, n->x, n->y);
      else
	drawNode(r, n->x, n->y);
      ++idx;
    }
  
  // Edges
  auto edges = g->getEdges();
  for (auto& e : edges)
    {
      drawEdge(r, nodes[e->a]->x, nodes[e->a]->y,
	       nodes[e->b]->x, nodes[e->b]->y, e->weight);
    }
}

/** Set the index of the node marked as the closest of the mouse
  *
  * \param idx The node index (from 0) or -1 if deactivated.
  *
  */
void
Drawer::setClosestNodeIndex(int idx)
{
  closestNodeIndex = idx;
}

void
Drawer::drawNode(SDL_Renderer* r, int x, int y)
{
  circleColor(r, x, y, circles_radius, 0xFF2222FF);

}

void
Drawer::drawClosestNode(SDL_Renderer* r,int x, int y)
{
  circleColor(r, x, y, circles_radius, 0xFF2222FF);
  filledCircleColor(r, x, y, circles_radius - 2, 0xFFD700FF);
}

void
Drawer::drawEdge(SDL_Renderer* r, int x1, int y1, int x2, int y2, int weight)
{
  int centerx, centery;

  SDL_SetRenderDrawColor( r, 80, 80, 80, 255 );
  SDL_RenderDrawLine(r, x1, y1, x2, y2);

  if (x1 > x2)
    centerx = x2 + ((x1-x2)/2);
  else
    centerx = x1 + ((x2-x1)/2);
  
  if (y1 > y2)
    centery = y2 + (abs(y1-y2)/2);
  else
    centery = y1 + (abs(y2-y1)/2);
  
  Text t(font, centerx, centery, to_string(weight), white);
  t.draw(r);
}

void
Drawer::printKey(SDL_Renderer* r, const string& k)
{
  SDL_Color red = {250, 50, 50};
  Text t(keyFont, 20, 10, k, red);
  t.draw(r);
}

void
Drawer::printMod(SDL_Renderer* r, const string& m)
{
  // Removed until we know how to make difference between key and mod
  /*
  SDL_Color red = {250, 50, 50};
  Text t(keyFont, 20, 10, m, red);
  t.draw(r);
  */
}
