[[_TOC_]]

	Note : je ne stream plus et la chaîne twitch liée n'existe plus., Ce
	projet n'est donc plus mis à jour régulièrerment.

# Stream

Un simple dépôt d'exemple pour streamer sur 
[twitch.tv/solebull](https://www.twitch.tv/solebull).

## Static code analysis

On utilisera `cppcheck` pour éviter certaines erreurs listées ici :
http://sourceforge.net/p/cppcheck/wiki/ListOfChecks 

# aafract

Un viewer de fractales utilisant SDL et aalib pour le rendu.
Ce projet a son propre [README](aafract/README.md).

# pathviz

## Dependencies

	sudo apt install make cmake g++ libsdl2-dev libsdl2-ttf-dev \
	  libsdl2-gfx-dev cppcheck check

## Building

Un visualizer de pathfinder en C++/SDL2

	mkdir build
	cd build/
	cmake ..
	make
	./pathviz
	
To generate doxygen documentation :

	doxygen Doxyfile-pathviz
	[browser] html-pathviz
