#ifndef __A_HPP__
#define __A_HPP__

struct As{
  int i;
};

void callA(const As*);

#endif  // !__A_HPP__
